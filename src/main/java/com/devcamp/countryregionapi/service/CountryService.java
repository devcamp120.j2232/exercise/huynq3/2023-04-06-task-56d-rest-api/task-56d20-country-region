package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;
@Service
public class CountryService extends RegionService{
    public ArrayList<Country> listCountries(){
        Country country1 = new Country("VN", "Vietnam", listRegionsVN());
        Country country2 = new Country("USA","America", listRegionsUSA());
        Country country3 = new Country("AUS","Australia", listRegionsAUS());
        Country country4 = new Country("RUS", "Nga", listRegionsTHA());
        Country country5 = new Country("FRA", "Phap", listRegionsCHN());
        Country country6 = new Country("CAN", "Canada", listRegionsCAN());
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);
        countries.add(country3);
        countries.add(country4);
        countries.add(country5);
        countries.add(country6);
        return countries;
    }
    public Country getCountry(int index){
        Country country = null;
        if (index >= 0 && index <= listCountries().size()){
            country = listCountries().get(index);
        }
        return country;
    }

}
